<?php

/*
			var ingressData = {
				portal_id: json[0],
				server_timestamp: json[1],
				data: json[2].plext
			};

			$.ajax({
				type: "POST",
				url: "https://dev.plainsight.ro/_ingress/watcher.php",
				crossDomain: true,
				data: ingressData
			});

 */
class IngressLogger {



    public function __construct()
    {
        $this->setHeaders();
//        include 'vendor/autoload.php';
//        $_propel = new \Propel\Runtime\Propel();
//        $_goutte = new Goutte\Client();
//        $_guzzle = new GuzzleHttp\Client();
    }

    public function setHeaders()
    {
        header('Access-Control-Allow-Origin: https://www.ingress.com');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key");
        header("Access-Control-Allow-Methods: *");
        header('Content-type: charset=utf-8');
    }

    public function replyWithHeaders()
    {
        if (isset($_POST) && !empty($_POST)) {
            http_response_code(200);
        } else {
            http_response_code(418);
        }
    }
}