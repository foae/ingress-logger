#Known issues with v1#
* Actions of the same kind (capture, field, link, destroy) that happen in the exact same second are discarded (due to a UNIQUE KEY constraint on the incoming NIA provided unix time) 
* Sometimes the JS taking care of the page _soft auto refresh_ hangs. Workaround: extension/plugin that makes a _hard refresh_ 

#How to setup#
* establish a 24/7 online server (VM, VPS) with Chrome/Firefox running. A Raspberry Pi 2 B+ should suffice. 
* Install TamperMonkey (Chrome/Chromium) or GraseMonkey (Firefox/Iceweasel) 
* Install **IITC**, **Periodic refresh** from the IITC website 
* Install the custom js script **`js/playerTrackerLogger.user.js`** From this point you should configure the AJAX link in the JS file to point to your HTTPS server. 
* Adapt to your needs and run the query from `old/db_maintenance.php`  
Done. 
 
###Debugging### 
* Dev Tools -> Console 
* Dev Tools -> Network and seek the custom POSTs 

###Planned for upcoming v2###
* use Guzzle instead of browser plugins 
* check out Goutte! 
* use Propel or Doctrine instead of vanilla PDO 
* POST directly to NIA API (getEntities and such) instead of relying on Intel Map for scraping 