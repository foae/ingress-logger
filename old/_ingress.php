<?php

/* Lower the gates a little */
//header('Access-Control-Allow-Origin: https://www.ingress.com');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key");
header("Access-Control-Allow-Methods: *");

if (isset($_POST)) {

	file_put_contents('_ingress.json', json_encode($_POST), FILE_APPEND | LOCK_EX);
	file_put_contents('_ingress.log', print_r($_POST, true), FILE_APPEND | LOCK_EX);

}

http_response_code(200);
