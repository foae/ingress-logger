<?php

/* Lower the gates a little */
header('Access-Control-Allow-Origin: https://www.ingress.com'); //header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key");
header("Access-Control-Allow-Methods: *");
header('Content-type: charset=utf-8');

/* DB shit */
define('DB_HOST', 'localhost');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', '');

/* Face all your mistakes */
define('DEBUG', true);
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

/* INFO */
if (isset($_GET['phpinfo'])) {
	phpinfo();
	die();
}

/**
START
*/
if (isset($_POST) && !empty($_POST)) :

    // $action = trim(rtrim(filter_var($_POST['markup'][1][1]['plain'], FILTER_SANITIZE_STRING)));
    $action = $_POST['text'];
	$player_name = trim(rtrim(filter_var($_POST['markup'][0][1]['plain'], FILTER_SANITIZE_STRING)));
	$player_faction = trim(rtrim(filter_var($_POST['markup'][0][1]['team'], FILTER_SANITIZE_STRING)));
    $portal_name = trim(rtrim($_POST['markup'][2][1]['name']));
    $portal_address = trim(rtrim($_POST['markup'][2][1]['address']));
    $portal_lat = trim(rtrim($_POST['markup'][2][1]['latE6']));
    $portal_long = trim(rtrim($_POST['markup'][2][1]['lngE6']));

    try {
        $pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = $pdo->query("SELECT * FROM `deploy` LIMIT 1");
    } catch (PDOException $e) {
        file_put_contents('error_db_connect.log', print_r($e, true), FILE_APPEND | LOCK_EX);
    }


	/*

	1. DEPLOY RESO

	*/
	if (strpos($action, 'deployed a Resonator') !== false) {

		$table = 'deploy';

		$stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long)
            VALUES(?,?,?,?,?,?)
        ");
		$result = $stmt->execute([$player_name, $player_faction, $portal_name, $portal_address, $portal_lat, $portal_long]);

        file_put_contents('deploy.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	/*

	2. LINK

	*/
	if (strpos($action, 'linked') !== false) {

		$table = 'link';

        // To
        $portal_name_linked = $_POST['markup'][4][1]['name'];
        $portal_address_linked = $_POST['markup'][4][1]['address'];
        $portal_lat_linked = $_POST['markup'][4][1]['latE6'];
        $portal_long_linked = $_POST['markup'][4][1]['lngE6'];

        $stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long, portal_name_linked, portal_address_linked, portal_lat_linked, portal_long_linked)
            VALUES (?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute([
            $player_name,$player_faction,$portal_name,$portal_address,$portal_lat,$portal_long,$portal_name_linked,$portal_address_linked,$portal_lat_linked,$portal_long_linked
        ]);


        file_put_contents('link.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	/*

	3. CREATE A CONTROL FIELD

	*/
	if (strpos($action, 'created a Control Field') !== false) {

		$table = 'field';

        $stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long, mu)
            VALUES(?,?,?,?,?,?,?)
        ");
		$result = $stmt->execute([$player_name, $player_faction, $portal_name, $portal_address, $portal_lat, $portal_long, $_POST['markup'][4][1]['plain']]);


        file_put_contents('field.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	/*

	4. CAPTURE PORTAL

	*/
	if (strpos($action, 'captured') !== false) {

		$table = 'capture';
        $stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long)
            VALUES(?,?,?,?,?,?)
        ");
		$result = $stmt->execute([$player_name, $player_faction, $portal_name, $portal_address, $portal_lat, $portal_long]);

        file_put_contents('capture.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	/*

	5. DESTROY RESO

	*/
	if (strpos($action, 'destroyed a Resonator') !== false) {

		$table = 'destroy_reso';

        $stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long)
            VALUES(?,?,?,?,?,?)
        ");
		$result = $stmt->execute([$player_name, $player_faction, $portal_name, $portal_address, $portal_lat, $portal_long]);


        file_put_contents('destroy_reso.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	/*

	6. DESTROY LINK

	*/
	if (strpos($action, 'destroyed the Link') !== false) {

		$table = 'destroy_link';

        // To
        $portal_name_linked = $_POST['markup'][4][1]['name'];
        $portal_address_linked = $_POST['markup'][4][1]['address'];
        $portal_lat_linked = $_POST['markup'][4][1]['latE6'];
        $portal_long_linked = $_POST['markup'][4][1]['lngE6'];

        $stmt = $pdo->prepare("
            INSERT INTO {$table} (player_name, player_faction, portal_name, portal_address, portal_lat, portal_long, portal_name_linked, portal_address_linked, portal_lat_linked, portal_long_linked)
            VALUES (?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute([
            $player_name,$player_faction,$portal_name,$portal_address,$portal_lat,$portal_long,$portal_name_linked,$portal_address_linked,$portal_lat_linked,$portal_long_linked
        ]);

        file_put_contents('destroy_link.log', print_r($result, true), FILE_APPEND | LOCK_EX);
	}


	//file_put_contents('__test.json', json_encode($_POST), FILE_APPEND | LOCK_EX);
	//file_put_contents('__test.log', print_r($_POST, true), FILE_APPEND | LOCK_EX);


endif;

http_response_code(200);
