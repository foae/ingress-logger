<?php

/*
 * Clean it all up
 */
$clean_db = "
delete from capture;
delete from deploy;
delete from destroy_link;
delete from destroy_field;
delete from destroy_reso;
delete from field;
delete from link;

ALTER TABLE capture AUTO_INCREMENT = 1;
ALTER TABLE deploy AUTO_INCREMENT = 1;
ALTER TABLE destroy_link AUTO_INCREMENT = 1;
ALTER TABLE destroy_field AUTO_INCREMENT = 1;
ALTER TABLE destroy_reso AUTO_INCREMENT = 1;
ALTER TABLE field AUTO_INCREMENT = 1;
ALTER TABLE link AUTO_INCREMENT = 1;
";

/*
 * TODO: don't hardcode the DB name, let the user choose
 */
$create_db = "
-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `admin_ingress` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `admin_ingress`;

CREATE TABLE `capture` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `deploy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `destroy_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `mu` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `destroy_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `portal_name_linked` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address_linked` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat_linked` int(11) unsigned NOT NULL,
  `portal_long_linked` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_street_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `destroy_reso` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `mu` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingress_timestamp` bigint(20) unsigned NOT NULL,
  `portal_guid` varchar(254) COLLATE utf8_bin NOT NULL,
  `local_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `player_faction` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_name` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat` int(11) unsigned NOT NULL,
  `portal_long` int(11) unsigned NOT NULL,
  `portal_name_linked` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_address_linked` varchar(254) COLLATE utf8_bin NOT NULL,
  `portal_lat_linked` int(11) unsigned NOT NULL,
  `portal_long_linked` int(11) unsigned NOT NULL,
  `raw_log` text COLLATE utf8_bin,
  `portal_street` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_street_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_city_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  `portal_country_linked` varchar(254) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingress_timestamp` (`ingress_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- 2016-01-04 17:14:26
";