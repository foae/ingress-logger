<?php

/*
 * Status: draft
 */

$total_portals_captured = "
SELECT
player_name, left(player_faction, 3) as faction,
count(*) as total_portals_captured
FROM `capture`
group by player_name order by total_portals_captured desc limit 10;
";

$total_mu_destroyd = "
SELECT
player_name, left(player_faction, 3) as faction,
sum(mu) as mu_destroyed
FROM `destroy_field`
group by player_name order by mu_destroyed desc limit 10
";

$total_mu_captured = "
SELECT
player_name, left(player_faction, 3) as faction,
sum(mu) as mu_captured
FROM `field`
group by player_name order by mu_captured desc limit 10
";

$total_links_made = "
SELECT
player_name, left(player_faction, 3) as faction,
count(*) as total_links
FROM `link`
group by player_name order by total_links desc limit 10
";

$total_links_destroyed = "
SELECT
player_name, left(player_faction, 3) as faction,
count(*) as total_links_destroyed
FROM `destroy_link`
group by player_name order by total_links_destroyed desc limit 10
";