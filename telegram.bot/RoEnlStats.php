<?php

/*
 * config.php
 */
if (file_exists('../config.php')) {
    require_once '../config.php';
} else {
    http_response_code(500);
    die('Config file not found.');
}

/*
 * Composer
 */
if (file_exists('../vendor')) {
    include '../vendor/autoload.php';
}

/*
 * https://api.telegram.org/bot<token>/METHOD_NAME
 */
$tg_request_url = 'https://api.telegram.org/bot' . $token .'/';

function setWebhook($url) {
    // Open connection
    $ch = curl_init();

    $webhookUrl = $url . 'setWebhook';

    // Set the url, number of POST vars, POST data etc.
    curl_setopt($ch, CURLOPT_URL, $webhookUrl);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['url' => $webhookUrl]));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

    // Exec
    $result = curl_exec($ch);
    writeToDisk('_curl_request', $result);

    // Close
    curl_close($ch);
}























/* ------------------------------------------------------------------------------------------------------
 * HELPERS
 * ------------------------------------------------------------------------------------------------------
 */
if (isset($_POST) && !empty($_POST)) {
    writeToDisk('_post', $_POST);
} else if (isset($_GET) && !empty($_GET)) {
    writeToDisk('_get', $_GET);
} else if (isset($_GET) && $_GET['action'] === 'setWebhook') {
    setWebhook($tg_request_url);
}

function writeToDisk($file_name, $var) {
    file_put_contents($file_name . '.log', print_r($var, true), FILE_APPEND | LOCK_EX);
    file_put_contents($file_name . '.json', json_encode($var), FILE_APPEND | LOCK_EX);
}