<?php

/*
 * config.php
 * * defines DB_HOST, DB_NAME, DB_USER, DB_PASS
 */
if (file_exists('config.php')) {
    require_once 'config.php';
} else {
    http_response_code(500);
    die('Config file not found. Cannot continue.');
}

/*
 * Debug?
 */
define('DEBUG', true);
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
if (isset($_GET['phpinfo'])) {
    phpinfo();
    die();
}

/*
 * Composer
 */
if (file_exists('vendor')) {
    include 'vendor/autoload.php';
}

/*
 * Set the headers
 */
header('Access-Control-Allow-Origin: *'); // narrow it down to www.ingress.com
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key");
header("Access-Control-Allow-Methods: *");
header('Content-type: charset=utf-8');

/**
 * START
 */
if (isset($_POST) && !empty($_POST)) {

    $action = $_POST['data']['text'];
    $raw_log = trim(rtrim($_POST['data']['text'])); // for now

    $ingress_timestamp = trim(rtrim($_POST['ingress_timestamp']));
    $portal_guid = trim(rtrim($_POST['portal_guid']));

    $player_name = trim(rtrim($_POST['data']['markup'][0][1]['plain']));
    $player_faction = trim(rtrim($_POST['data']['markup'][0][1]['team']));

    $portal_name = trim(rtrim($_POST['data']['markup'][2][1]['name']));
//    $portal_faction = trim(rtrim($_POST['data']['markup'][2][1]['team']));
    $portal_address = trim(rtrim($_POST['data']['markup'][2][1]['address']));
    $portal_lat = trim(rtrim($_POST['data']['markup'][2][1]['latE6']));
    $portal_long = trim(rtrim($_POST['data']['markup'][2][1]['lngE6']));

    $portal_street = '';
    $portal_city = '';
    $portal_country = '';
    $portal_street_linked = '';
    $portal_city_linked = '';
    $portal_country_linked = '';


    /*
     * Attempt to break the address into
     * * street + no
     * * city
     * * country
     */
    if (strpos(strtolower($portal_address), 'unknown') === false) {
        $addr = explode(',', $portal_address);

        /* NIA fuck your data inconsistency */
        if (count($addr) > 3) {
            $portal_street = trim(rtrim($addr[0])) . ', ' . trim(rtrim($addr[1]));
            $portal_city = trim(rtrim($addr[2]));
            $portal_country = trim(rtrim($addr[3]));
        } else {
            $portal_street = trim(rtrim($addr[0]));
            $portal_city = trim(rtrim($addr[1]));
            $portal_country = trim(rtrim($addr[2]));
        }
    }

    /*
     * DB
     */
    try {
        $pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // $result = $pdo->query("SELECT * FROM `deploy` LIMIT 1");
    } catch (PDOException $e) {
        file_put_contents('error_db_connect.log', print_r($e, true), FILE_APPEND | LOCK_EX);
    }

    /*
     * -----------------------------------------------------------------------------------------------------
     * BEGIN ACTIONS
     * -----------------------------------------------------------------------------------------------------
     */

    /*
     * 1. DEPLOY RESO
     */
    if (strpos($action, 'deployed a Resonator') !== false) {

        $table = 'deploy';
        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                raw_log,
                portal_street,
                portal_city,
                portal_country
            )
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country
            ]
        );

    }


    /*
     * 2. LINK
     */
    if (strpos($action, 'linked') !== false) {

        $table = 'link';

        // To
        $portal_name_linked = $_POST['data']['markup'][4][1]['name'];
        $portal_address_linked = $_POST['data']['markup'][4][1]['address'];
        $portal_lat_linked = $_POST['data']['markup'][4][1]['latE6'];
        $portal_long_linked = $_POST['data']['markup'][4][1]['lngE6'];

        /*
         * Attempt to break the LINKED address into
         * * street + no
         * * city
         * * country
         */
        if (strpos(strtolower($portal_address_linked), 'unknown') === false) {
            $addr_linked = explode(',', $portal_address_linked);
            /* NIA fuck your data inconsistency */
            if (count($addr_linked) > 3) {
                $portal_street_linked = trim(rtrim($addr_linked[0])) . ', ' . trim(rtrim($addr_linked[1]));
                $portal_city_linked = trim(rtrim($addr_linked[2]));
                $portal_country_linked = trim(rtrim($addr_linked[3]));
            } else {
                $portal_street_linked = trim(rtrim($addr_linked[0]));
                $portal_city_linked = trim(rtrim($addr_linked[1]));
                $portal_country_linked = trim(rtrim($addr_linked[2]));
            }
        }

        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                portal_name_linked,
                portal_address_linked,
                portal_lat_linked,
                portal_long_linked,
                raw_log,
                portal_street,
                portal_city,
                portal_country,
                portal_street_linked,
                portal_city_linked,
                portal_country_linked
            )
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $portal_name_linked,
                $portal_address_linked,
                $portal_lat_linked,
                $portal_long_linked,
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country,
                $portal_street_linked,
                $portal_city_linked,
                $portal_country_linked
            ]
        );

    }


    /*
     * 3. CREATE A CONTROL FIELD
     */
    if (strpos($action, 'created a Control Field') !== false) {

        $table = 'field';

        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                mu,
                raw_log,
                portal_street,
                portal_city,
                portal_country
            )
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $_POST['data']['markup'][4][1]['plain'],
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country
            ]
        );

    }


    /*
     * 3.A. DESOTRY A CONTROL FIELD
     */
    if (strpos($action, 'destroyed a Control Field') !== false) {

        $table = 'destroy_field';
        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                mu,
                raw_log,
                portal_street,
                portal_city,
                portal_country
            )
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $_POST['data']['markup'][4][1]['plain'],
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country
            ]
        );

    }


    /*
     * 4. CAPTURE PORTAL
     */
    if (strpos($action, 'captured') !== false) {

        $table = 'capture';
        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                raw_log,
                portal_street,
                portal_city,
                portal_country
            )
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country
            ]
        );

    }


    /*
     * 5. DESTROY RESO
     */
    if (strpos($action, 'destroyed a Resonator') !== false) {

        $table = 'destroy_reso';
        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                raw_log,
                portal_street,
                portal_city,
                portal_country
            )
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country
            ]
        );

    }


    /*
     * 6. DESTROY LINK
     */
    if (strpos($action, 'destroyed the Link') !== false) {

        $table = 'destroy_link';

        // To
        $portal_name_linked = $_POST['data']['markup'][4][1]['name'];
        $portal_address_linked = $_POST['data']['markup'][4][1]['address'];
        $portal_lat_linked = $_POST['data']['markup'][4][1]['latE6'];
        $portal_long_linked = $_POST['data']['markup'][4][1]['lngE6'];


        /*
         * Attempt to break the LINKED address into
         * * street + no
         * * city
         * * country
         */
        if (strpos(strtolower($portal_address_linked), 'unknown') === false) {
            $addr_linked = explode(',', $portal_address_linked);
            /* NIA fuck your data inconsistency */
            if (count($addr_linked) > 3) {
                $portal_street_linked = trim(rtrim($addr_linked[0])) . ', ' . trim(rtrim($addr_linked[1]));
                $portal_city_linked = trim(rtrim($addr_linked[2]));
                $portal_country_linked = trim(rtrim($addr_linked[3]));
            } else {
                $portal_street_linked = trim(rtrim($addr_linked[0]));
                $portal_city_linked = trim(rtrim($addr_linked[1]));
                $portal_country_linked = trim(rtrim($addr_linked[2]));
            }
        }

        $stmt = $pdo->prepare("
            INSERT INTO {$table}
            (
                ingress_timestamp,
                portal_guid,
                player_name,
                player_faction,
                portal_name,
                portal_address,
                portal_lat,
                portal_long,
                portal_name_linked,
                portal_address_linked,
                portal_lat_linked,
                portal_long_linked,
                raw_log,
                portal_street,
                portal_city,
                portal_country,
                portal_street_linked,
                portal_city_linked,
                portal_country_linked
            )
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        ");
        $result = $stmt->execute(
            [
                $ingress_timestamp,
                $portal_guid,
                $player_name,
                $player_faction,
                $portal_name,
                $portal_address,
                $portal_lat,
                $portal_long,
                $portal_name_linked,
                $portal_address_linked,
                $portal_lat_linked,
                $portal_long_linked,
                $raw_log,
                $portal_street,
                $portal_city,
                $portal_country,
                $portal_street_linked,
                $portal_city_linked,
                $portal_country_linked
            ]
        );

    }

    /*
     * File loggers
     */
    //file_put_contents('_ingress_logger.json', json_encode($_POST), FILE_APPEND | LOCK_EX);
    //file_put_contents('_ingress_logger.log', print_r($_POST, true), FILE_APPEND | LOCK_EX);


    /*
     * Offer some final feedback
     */
    http_response_code(200);
    //echo '<pre>';
    //print_r(get_defined_vars());
    die();
}

/*
 * Easter egg
 */
http_response_code(418);
//echo "I'm a teapot.";
//echo '<pre>' . PHP_EOL;
//print_r(get_defined_vars());

//file_put_contents('_debug.json', json_encode($_POST), FILE_APPEND | LOCK_EX);
//file_put_contents('_debug.log', print_r($_POST, true), FILE_APPEND | LOCK_EX);
